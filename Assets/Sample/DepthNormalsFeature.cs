﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class DepthNormalsFeature : ScriptableRendererFeature
{
    public LayerMask layers = 0;
    
    DepthNormalsPass depthNormalsPass;
    RenderTargetHandle depthNormalsTextureHandle;
    Material depthNormalsMaterial;

    public override void Create()
    {
        // Рендерить этим Build-in шейдером
        depthNormalsMaterial = CoreUtils.CreateEngineMaterial("Hidden/Internal-DepthNormalsTexture");
        
        // Создаем наш пасс
        depthNormalsPass = new DepthNormalsPass(RenderQueueRange.opaque, layers, depthNormalsMaterial);
        
        // Вызвать до основного рендеринга
        depthNormalsPass.renderPassEvent = RenderPassEvent.AfterRenderingPrePasses;
        
        // Инитим параметры текстуры
        depthNormalsTextureHandle.Init("_CameraDepthNormalsTexture");
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        // Внедряем наш пасс
        depthNormalsPass.Setup(renderingData.cameraData.cameraTargetDescriptor, depthNormalsTextureHandle);
        renderer.EnqueuePass(depthNormalsPass);
    }
}
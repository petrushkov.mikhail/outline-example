using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class DepthNormalsPass : ScriptableRenderPass
{
    readonly string m_ProfilerTag = "My DepthNormals Prepass";
    readonly ShaderTagId m_ShaderTagId = new ShaderTagId("DepthOnly");

    private RenderTargetHandle depthNormalsTextureHandle;
    private RenderTextureDescriptor cameraDescriptor;

    private Material depthNormalsMaterial;
    private FilteringSettings filteringSettings;

    public DepthNormalsPass(RenderQueueRange renderQueueRange, LayerMask layerMask, Material material)
    {
        // Настройка того, какие объекты мы будем отрисовывать (слой, прозрачные/непрозрачные)
        filteringSettings = new FilteringSettings(renderQueueRange, layerMask);

        // Сохраняем встроенный материал, который depth рисует
        depthNormalsMaterial = material;
    }

    public void Setup(RenderTextureDescriptor cameraDescriptor, RenderTargetHandle depthNormalsTextureHandle)
    {
        // сохраняем ссылку на текущую камеру
        this.cameraDescriptor = cameraDescriptor;

        // сохраняем нужное нам имя текстуры
        this.depthNormalsTextureHandle = depthNormalsTextureHandle;
    }

    public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
    {
        // Метод GetTemporaryRT () создаёт временную RenderTexture с заданными параметрами
        // и устанавливает её как глобальное свойство шейдера с указанным именем.
        cmd.GetTemporaryRT(depthNormalsTextureHandle.id, cameraDescriptor);
        ConfigureTarget(depthNormalsTextureHandle.Identifier());
        ConfigureClear(ClearFlag.All, Color.black);
    }

    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
    {
//         Создаем буффер команд, который можно будет видеть во Frame debugger как группу
        CommandBuffer cmd = CommandBufferPool.Get(m_ProfilerTag);
        using (new ProfilingSample(cmd, m_ProfilerTag))
        {
            context.ExecuteCommandBuffer(cmd);
            cmd.Clear();

            // Настраиваем, что и как рисовать
            var sortFlags = renderingData.cameraData.defaultOpaqueSortFlags;
            var drawSettings = CreateDrawingSettings(m_ShaderTagId, ref renderingData, sortFlags);

            // Говорим, что использовать встроенный материал для глубины
            drawSettings.overrideMaterial = depthNormalsMaterial;

            // рисуем только то, что видно на экране - результат cull
            context.DrawRenderers(renderingData.cullResults, ref drawSettings,
                ref filteringSettings);
        }

        context.ExecuteCommandBuffer(cmd);
        CommandBufferPool.Release(cmd);
    }

    public override void FrameCleanup(CommandBuffer cmd)
    {
        if (depthNormalsTextureHandle != RenderTargetHandle.CameraTarget)
        {
            cmd.ReleaseTemporaryRT(depthNormalsTextureHandle.id);
            depthNormalsTextureHandle = RenderTargetHandle.CameraTarget;
        }
    }
}